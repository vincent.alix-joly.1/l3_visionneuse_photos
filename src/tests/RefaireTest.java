package tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.LinkedList;

import org.junit.Test;

import commandes.Commande;
import commandes.Glisser;
import commandes.Refaire;

public class RefaireTest {

    Refaire r = new Refaire();
    LinkedList<Commande> historique = new LinkedList<>();

    @Test
    public void executerVide() {
        assertEquals(0, r.executer(historique, 0));
    }

    @Test
    public void executerPremiereInvocation() {
        peuplerHistorique(3);
        assertEquals(0, r.executer(historique, 0));
    }

    @Test
    public void executerTamponPlein() {
        peuplerHistorique(3);
        assertEquals(1, r.executer(historique, 2));
    }

    @Test
    public void executerTeteInvalide() {
        peuplerHistorique(3);
        assertEquals(2, r.executer(historique, 5));
    }

    private void peuplerHistorique(int nbCommandes) {
        for (int i = 0; i < nbCommandes; i++) {
            historique.add(new Glisser());
        }
    }
}
