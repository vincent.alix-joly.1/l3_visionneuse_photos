package tests;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.LinkedList;

import org.junit.Test;

import controleurs.TamponCommandes;

public class TamponCommandesTest {

    @Test
    public void tronquerHistoriqueTest() {
        int maxSize = 4;
        LinkedList<Integer> hist = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            hist.add(i);
        }
        var expected = hist.subList(0, maxSize).toArray();

        TamponCommandes.tronquerFinHistorique(hist, maxSize);

        assertArrayEquals(expected, hist.toArray());
    }

    @Test
    public void clampAddition0_0_0_0() {
        assertEquals(0, TamponCommandes.clampAddition(0, 0, 0, 0));
    }

    @Test
    public void clampAddition0_1_0_1() {
        assertEquals(1, TamponCommandes.clampAddition(0, 1, 0, 1));
    }

    @Test
    public void clampAddition0_2_0_1() {
        assertEquals(1, TamponCommandes.clampAddition(0, 2, 0, 1));
    }

    @Test
    public void clampAddition0_m2_0_1() {
        assertEquals(0, TamponCommandes.clampAddition(0, -2, 0, 1));
    }

    @Test
    public void clampAdditionListe() {
        int[] liste = { 1, 1, 1, 1, 1, 1 };
        assertEquals(5, TamponCommandes.clampAddition(5, 1, 0, liste.length - 1));
    }

    @Test
    public void clampAdditionListeDepassement() {
        int[] liste = { 1, 1, 1, 1, 1, 1 };
        assertEquals(6, TamponCommandes.clampAddition(5, 1, 0, liste.length));
    }
}
