package commandes;

import java.awt.AWTEvent;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Function;

import modeles.ImageModifiable;

/**
 * Classe abstraite permettant d'operer des modifications sur une image
 * modifiable
 */
public abstract class Commande implements Cloneable {

    /**
     * nom utilisable par un ActionEvent et sa propriete ActionCommand pour
     * declencher une commande Defaire
     */
    public static final String ACTION_DEFAIRE = "DEFAIRE";
    
    /**
     * nom utilisable par un ActionEvent et sa propriete ActionCommand pour
     * declencher une commande Refaire
     */
    public static final String ACTION_REFAIRE = "REFAIRE";

    protected ImageModifiable imageModifiable;
    protected Function<AWTEvent, Boolean> declencheur;
    protected BiConsumer<AWTEvent, Commande> interpreteur;

    /**
     * cree une nouvelle commande
     */
    protected Commande() {
    }

    /**
     * change l'image modifiable modifiable qui sera affectee par l'execution de la
     * commande
     * 
     * @param imageModifiable l'image qu'affectera la commande
     */
    public void setImageModifiable(ImageModifiable imageModifiable) {
        this.imageModifiable = imageModifiable;
    }

    /**
     * recupere l'image modifiable affectee
     * 
     * @return l'image modifiable
     */
    public ImageModifiable getImageModifiable() {
        return imageModifiable;
    }

    /**
     * methode verifiant si l'event satisfait les conditions de declanchement de la
     * commande
     * 
     * @param e l'evenement a tester
     * @return true si l'evenement est capable de declencher cette commmande, faux
     *         autrement
     */
    public final boolean verifierEventValide(AWTEvent e) {
        if (declencheur == null)
            return false;
        else
            return declencheur.apply(e);
    }

    /**
     * utilise l'event pour parametrer la commande
     * 
     * @param e
     */
    public final void interpreterEvent(AWTEvent e) {
        interpreteur.accept(e, this);
    }

    /**
     * clone la commande
     */
    @Override
    public Object clone() {
        try {
            var clone = (Commande) super.clone();
            return clone;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * execute la commande et retourne si l'execution a modifie la pile
     * 
     * @param historique liste des commandes, la plus recente etant en tete de liste
     * @param teteQueue  index de la derniere commande dont le resultat est
     *                   actuellement affiche
     * @return 1 si la commande fait avancer la tete de l'historique, -1 si la
     *         commande fait reculer la tete de pile, 0 autrement
     */
    public abstract int executer(List<Commande> historique, int teteQueue);

    /**
     * methode facilitant le retrait d'un certain nombre d'entrees a la tete de la
     * liste de commandes. Les commandes sont retirees uniquement si le nombre de
     * troncatures est inferieur ou egal a la taille de la liste
     * 
     * @param liste         liste dont on doit tronquer des conmmandes
     * @param nbTroncatures nombre d'elements que l'on souhaite retirer
     */
    public static void tronquerDebutListe(List<Commande> liste, int nbTroncatures) {
        for (int i = 0; i < nbTroncatures && liste.size() >= nbTroncatures; i++) {
            liste.remove(0);
        }
    }

    /**
     * méthode retournant l'element en tete de liste s'il existe retourne
     * 
     * @param liste liste dont on souhaite obtenir l'element 0
     * @return l'element 0 s'il existe, null autrement
     */
    public static Commande getDessusListe(List<Commande> liste) {
        if (liste.size() > 0)
            return liste.get(0);
        else
            return null;
    }

    @Override
    public abstract boolean equals(Object obj);
}
