package commandes;

import java.awt.AWTEvent;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.SwingUtilities;

/**
 * commande permettant de faire glisser l'image dans sa perspective
 */
public class Glisser extends Commande {

    private Point posSouris;
    private Point posImage;

    /**
     * construit un glissement avec le declencheur et interpreteur par defaut
     */
    public Glisser() {
        super();

        declencheur = (AWTEvent e) -> {
            if (e instanceof MouseEvent && SwingUtilities.isLeftMouseButton((MouseEvent) e)
                    && e.getID() == MouseEvent.MOUSE_DRAGGED){
                return true;
            }else if (e instanceof MouseEvent && SwingUtilities.isLeftMouseButton((MouseEvent) e)
                    && e.getID() == MouseEvent.MOUSE_PRESSED){
                posSouris = ((MouseEvent) e).getPoint();
                return false;
            }
            else
                return false;
        };

        interpreteur = (AWTEvent e, Commande c) -> {
            int dragX = ((MouseEvent) e).getPoint().x - posSouris.x;
            int dragY = ((MouseEvent) e).getPoint().y - posSouris.y;
            posImage = ((MouseEvent) e).getComponent().getLocation();
            posImage.x += dragX;
            posImage.y += dragY;
        };
    }

    /**
     * execute le glissement. s'ajoute à l'historique si le glissement commence, ou
     * met a jour le glissement en cours
     * 
     * @param historique historique a mettre a jour
     * @param teteQueue  index de la derniere commande dont la transformation est
     *                   visible
     * @return 0
     */
    @Override
    public int executer(List<Commande> historique, int teteQueue) {
        boolean listeTronquee = false;
        if (teteQueue > 0) {
            Commande.tronquerDebutListe(historique, teteQueue);
            listeTronquee = true;
        }

        Commande derniereCommande = getDessusListe(historique);

        // fusionne la présente commande à la dernière si la commande destination est
        // "neuve" et du même type
        if (equals(derniereCommande) && !listeTronquee) {
        } else {
            historique.add(0, this);
            // depart = posSouris;
        }

        if (posImage != null)
            imageModifiable.setPosition(posImage);

        return 0;
    }

    /**
     * determine si l'objet est un autre glissement sur la meme image
     * 
     * @param obj objet a comparer
     * @return true si l'objet est de classe Glisser et si l'image modifiable pointe
     *         vers la meme instance
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof Glisser && ((Glisser) obj).getImageModifiable() == imageModifiable;
    }

}
