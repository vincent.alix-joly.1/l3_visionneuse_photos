package commandes;

import java.awt.AWTEvent;
import java.awt.event.ActionEvent;
import java.util.List;

/**
 * Commande permettant de renverser les dernieres commandes executees
 */
public class Defaire extends Commande {

    /**
     * cree un nouveau Defaire utilisant le declencheur et l'interpreteur par defaut
     */
    public Defaire() {
        super();

        declencheur = (AWTEvent e) -> {
            if (e instanceof ActionEvent && ((ActionEvent) e).getActionCommand().equals(Commande.ACTION_DEFAIRE))
                return true;
            else
                return false;
        };

        interpreteur = (AWTEvent e, Commande c) -> {
        };
    }

    /**
     * execute le present Defaire, s'inserant dans l'historique et faisant avancer
     * l'historique s'il y a des commandes a annuler
     * 
     * @param historique historique qui doit etre defait
     * @param teteQueue  index de la derniere commande dont le resultat est visible
     * @return 1 si la derniere commande peut etre defaite, 0 autrement
     */
    @Override
    public int executer(List<Commande> historique, int teteQueue) {

        if (teteQueue + 1 <= historique.size()) {

            historique.add(teteQueue, this);
            return 1;
        } else
            return 0;
    }

    /**
     * Defaire n'est egal a aucune commande
     * 
     * @return false
     */
    @Override
    public boolean equals(Object obj) {
        return false;
    }

}
