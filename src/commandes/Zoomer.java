package commandes;

import java.awt.AWTEvent;
import java.awt.event.MouseWheelEvent;
import java.util.List;

/**
 * Commande representant un changement d'echelle de l'image
 */
public class Zoomer extends Commande {

    private int direction;
    private double echelle;

    /**
     * construit un zoom en utilisant le declencheur et interpreteur par defaut
     */
    public Zoomer() {
        super();

        declencheur = (AWTEvent e) -> {
            if (e instanceof MouseWheelEvent)
                return true;
            else
                return false;
        };

        interpreteur = (AWTEvent e, Commande c) -> {
            var r = -((MouseWheelEvent) e).getWheelRotation();

            if (r >= 0) {
                ((Zoomer) c).direction = 1;
            } else {
                ((Zoomer) c).direction = -1;
            }

            ((Zoomer) c).echelle = r * 0.1;
        };
    }

    /**
     * execute le zoom tel que configure
     * 
     * @param historique historique des dernieres commandes effectuees
     * @param teteQueue  index de la derniere commande dont l'effet est affiche
     * @return 0
     */
    @Override
    public int executer(List<Commande> historique, int teteQueue) {

        boolean listeTronquee = false;
        if (teteQueue > 0) {
            Commande.tronquerDebutListe(historique, teteQueue);
            listeTronquee = true;
        }

        Commande derniereCommande = getDessusListe(historique);
        // fusionne la présente commande à la dernière si la commande destination est
        // "neuve" et du même type
        if (this.equals(derniereCommande) && !listeTronquee) {
        } else {
            historique.add(0, this);
        }

        imageModifiable.ajouterEchelle(echelle);

        return 0;
    }

    /**
     * compare le present Zoomer avec un objet. L'objet est equivalent s'il est
     * aussi un zoom dans la meme direction sur la meme image
     * 
     * @param obj objet a comparer
     * @return true si l'objet est aussi un zoom de la meme direction sur la meme
     *         image
     */
    @Override
    public boolean equals(Object obj) {
        return obj instanceof Zoomer && ((Zoomer) obj).direction == direction
                && ((Zoomer) obj).getImageModifiable() == imageModifiable;
    }
}
