package commandes;

import java.awt.AWTEvent;
import java.awt.event.ActionEvent;
import java.util.List;

/**
 * Commande permettant de refaire une commande defaite
 */
public class Refaire extends Commande {

    /**
     * cree une nouvelle commande avec un declencheur et interpreteur par defaut
     */
    public Refaire() {
        super();
        declencheur = (AWTEvent e) -> {
            if (e instanceof ActionEvent && ((ActionEvent) e).getActionCommand().equals(Commande.ACTION_REFAIRE))
                return true;
            else
                return false;
        };

        interpreteur = (AWTEvent e, Commande c) -> {
        };
    }

    /**
     * execute la commande
     * 
     * @param historique historique dont on souhaite restaurer l'effet d'une
     *                   commande
     * @param teteQueue  index de la derniere commande dont l'effet est affiche
     * @return -1 si une commande peut être defaite, 0 sinon
     */
    @Override
    public int executer(List<Commande> historique, int teteQueue) {

        if (teteQueue - 1 >= 0)
            return -1;
        else
            return 0;
    }

    /**
     * compare le Refaire a un autre objet. Un refaire n'est egal a aucune autre
     * commande
     * 
     * @param obj objet a comparer
     * @return false
     */
    @Override
    public boolean equals(Object obj) {
        return false;
    }
}
