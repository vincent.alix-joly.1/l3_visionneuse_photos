package controleurs;

import java.util.LinkedList;
import java.util.List;

import commandes.*;
import modeles.ImageModifiable;



/**
 * Singleton centralisant l'exécution des commandes sur les images modifiables
 * et la conservation de l'historique des commandes
 */
public class TamponCommandes {

    private static final int HISTORIQUE_MAXSIZE = 30;

    private static TamponCommandes instance = new TamponCommandes();

    private LinkedList<Commande> histCommandes = new LinkedList<>();
    private LinkedList<Memento> histMementos = new LinkedList<>();

    private int indexMementoActif = 0;

    private TamponCommandes() {
    }

    public static TamponCommandes getInstance() {
        return instance;
    }

    public void executerCommande(Commande c) {

        ImageModifiable imageVoulue = c.getImageModifiable();
        if (imageVoulue != null) {

            // prise de memento uniquement si la dernière commande est neuve
            if (indexMementoActif <= 0 && !c.equals(getCommandeActive())) {

                histMementos.add(0, imageVoulue.sauvegarder());
            }

            var mouvVoulu = c.executer(histCommandes, indexMementoActif);

            indexMementoActif = clampAddition(indexMementoActif, mouvVoulu, 0, histMementos.size() - 1);
            if (mouvVoulu != 0 && histMementos.size() > 0 && imageVoulue != null) {
                imageVoulue.restaurer(getMementoActif());
            }

            int diff = histMementos.size() - histCommandes.size();
            if (diff > 0) {
                tronquerDebutHistorique(histMementos, histMementos.size() - histCommandes.size());
                indexMementoActif = 0;
            } 
            tronquerFinHistorique(histCommandes, HISTORIQUE_MAXSIZE);
            tronquerFinHistorique(histMementos, HISTORIQUE_MAXSIZE);

        }
    }

    public Memento getMementoActif() {
        if (histCommandes.size() != 0)
            return histMementos.get(indexMementoActif);
        else
            return null;
    }

    public Commande getCommandeActive() {
        if (histCommandes.size() != 0)
            return histCommandes.get(indexMementoActif);
        else
            return null;
    }

    public ImageModifiable getDerniereImageModifiee() {
        if (histCommandes.size() != 0)
            return histCommandes.get(indexMementoActif).getImageModifiable();
        else
            return null;
    }

    public static void tronquerDebutHistorique(List<?> hist, int nbTrim) {
        for (int i = nbTrim; i > 0; i--) {
            hist.remove(0);
        }
    }

    public static void tronquerFinHistorique(List<?> hist, int maxSize) {
        for (int i = hist.size() - maxSize; i > 0; i--) {
            hist.remove(hist.size() - 1);
        }
    }

    public static int clampAddition(int valInit, int addition, int borneInf, int borneSup) {
        if (borneInf <= valInit + addition && valInit + addition <= borneSup) {
            return valInit += addition;
        } else if (borneInf > valInit + addition) {
            return borneInf;
        } else {
            return borneSup;
        }
    }
}
