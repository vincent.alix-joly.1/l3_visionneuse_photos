package controleurs;

import java.awt.AWTEvent;
import java.util.ArrayList;

import commandes.Commande;
import commandes.Defaire;
import commandes.Glisser;
import commandes.Refaire;
import commandes.Zoomer;

/**
 * Registre des prototypes de commandes. 
 * 
 * @see Commande
 */
public class InterpreteurAction extends ArrayList<Commande> {

    private static final InterpreteurAction instance = new InterpreteurAction(
            new Commande[] { new Defaire(), new Refaire(), new Glisser(), new Zoomer() });

    /**
     * Construit l'interpreteur par defaut a partir d'un tableau des prototypes de
     * commandes
     * 
     * @param prototypes Tableau des {@link Commande} originelles
     */
    private InterpreteurAction(Commande[] prototypes) {
        for (var p : prototypes) {
            add(p);
        }
    }

    /**
     * Recoit une action generee par l'utiisateur et, si elle correspond au
     * declencheur de l'une des commandes, clone, parametre et retourne la commande.
     * 
     * @param e evenement brut genere par les actions de l'usager
     * @return un clone du prototype de commande auquel correspond l'evenement brut,
     *         ou null si l'evenement ne correspond a aucune commande
     */
    public Commande interpreterAction(AWTEvent e) {
        for (Commande commande : this) {
            if (commande.verifierEventValide(e)) {
                var output = (Commande) commande.clone();
                output.interpreterEvent(e);
                return output;
            }
        }
        return null;
    }

    /**
     * retourne l'instance unique de l'interpreteur d'actions
     * 
     * @return l'instance d'InterpreteurAction
     */
    public static InterpreteurAction getInstance() {
        return instance;
    }

}
