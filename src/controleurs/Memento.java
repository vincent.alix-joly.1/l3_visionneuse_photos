package controleurs;

/**
 * interface signifiant qu'un objet est capable de sauvegarder son etat et de le
 * restituer plus tard
 */
public interface Memento {

    /**
     * cree un nouveau memento de l'etat actuel de l'instance concrete
     * 
     * @return nouveau Memento de l'instance concrete
     */
    public Memento sauvegarder();

    /**
     * restitue un etat precedant de l'objet grace a un memento
     * 
     * @param m memento que l'on desire restituer
     */
    public void restaurer(Memento m);
}
