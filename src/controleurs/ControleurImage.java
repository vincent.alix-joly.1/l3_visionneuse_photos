package controleurs;

import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import commandes.Commande;
import modeles.ImageModifiable;

/**
 * classe réagissant a tous les types d'evenements et laissant l'interpretation
 * de ces evenenements a {@link InterpreteurAction}
 * 
 * @see InterpreteurAction
 */
public class ControleurImage implements MouseMotionListener, MouseWheelListener, MouseListener, KeyListener {

    private final InterpreteurAction interprete;
    private final ImageModifiable modele;
    private final TamponCommandes receveur;

    /**
     * Cree un nouveau controleur
     * 
     * @param modele     ImageModifiable sur laquelle seront effectuee les commandes
     *                   recues
     * @param interprete InterpreteurAction appele pour interpreter les actions de
     *                   l'utilisateur et recuperer les commandes que ces actions
     *                   entrainent
     * @param receveur   TamponCommandes qui recevra les commandes generees et qui
     *                   les executera
     */
    public ControleurImage(ImageModifiable modele, InterpreteurAction interprete, TamponCommandes receveur) {
        this.modele = modele;
        this.interprete = interprete;
        this.receveur = receveur;
    }

    /**
     * methode centralisant l'appel de l'interpreteur et du receveur lorsqu'une
     * action est effectuee par l'usager
     * 
     * @param e InputEvent genere par l'utilisateur sur la vue
     */
    public void controllerAction(InputEvent e) {

        Commande c = interprete.interpreterAction(e);
        if (c != null) {
            c.setImageModifiable(modele);
            this.receveur.executerCommande(c);
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        controllerAction(e);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        controllerAction(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        controllerAction(e);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        controllerAction(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        controllerAction(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        controllerAction(e);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        controllerAction(e);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        controllerAction(e);
    }

    @Override
    public void mouseWheelMoved(MouseWheelEvent e) {
        controllerAction(e);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        controllerAction(e);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        controllerAction(e);
    }

}
