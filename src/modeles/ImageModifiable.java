package modeles;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashSet;
import javax.imageio.ImageIO;
import communs.Observable;
import communs.Observateur;
import controleurs.Memento;

/**
 * modele representant une image et les transformations geometriques que l'on y
 * applique
 */
public class ImageModifiable implements Cloneable, Serializable, Memento, Observable, Observateur {

    private static final Boolean REQUIS = true;
    private ImageModifiable imgSource;
    private transient BufferedImage source;
    private double echelle = 1.0;
    private Point position = new Point(0,0);
    private Boolean positionReset = REQUIS;
    private AffineTransform transformation = new AffineTransform();
    private final HashSet<Observateur> observateurs = new HashSet<>();
    private Dimension dimensionDepart;
    private Dimension dimensionActuel;


    /**
     * cree une image modifiable en se servant de l'image specifie comme base
     * 
     * @param source image qui sera transformee par ce modele
     */
    public ImageModifiable(BufferedImage source) {
        this.source = source;
    }

    /**
     * Cree une image modifiable en surveillant une autre, permettant
     * potentiellement de chaîner les changements. L'image source sera l'image
     * modifiee du param
     * 
     * @param imgSource ImageModifiable sur laquelle sera basee la nouvelle instance
     */
    public ImageModifiable(ImageModifiable imgSource) {
        if (imgSource != null) {
            this.imgSource = imgSource;
            this.source = imgSource.getImageModifiee();
            imgSource.ajouterObservateur(this);
        }
    }

    /**
     * ajoute un transformation affine a l'image et averti ses observateurs
     * 
     * @param t AffineTransform a concatener a this.transformation
     */
    public void ajouterTransformation(AffineTransform t) {
        transformation.concatenate(t);
        notifierObservateurs();
    }

    /**
     * applique la transformation affine concatenee a l'image source et retourne le
     * resultat de la transformation
     */
    public BufferedImage getImageModifiee() {
        var scaled = source.getScaledInstance(
                (int) (source.getWidth(null) * echelle >= 1 ? source.getWidth(null) * echelle : 1),
                (int) (source.getHeight(null) * echelle >= 1 ? source.getHeight(null) * echelle : 1),
                BufferedImage.SCALE_REPLICATE);

        BufferedImage output = new BufferedImage(scaled.getWidth(null), scaled.getHeight(null),
                BufferedImage.TYPE_INT_RGB);
        output.getGraphics().drawImage(scaled, 0, 0, null);
        return output;
    }

    /**
     * change l'image source de l'instance et avertit les observateurs
     * 
     * @param source nouvelle image source
     */
    public void setSource(BufferedImage source) {
        this.source = source;
        echelle = 1.0;
        dimensionActuel = (Dimension) dimensionDepart.clone();
        positionReset = REQUIS;
        setPositionDepart();
        notifierObservateurs();
    }

    /**
     * retourne la position en X du coin superieur gauche de l'image
     * 
     * @return la position en X
     */
    public int getX() {
        return position.x;
    }

    /**
     * retourne la position en Y du coin superieur gauche de l'image
     * 
     * @return la position en Y
     */
    public int getY() {
        return position.y;
    }

    /**
     * change la position du coin superieur gauche de l'image
     * 
     * @param p le nouveau Point du coin
     */
    public void setPosition(Point p) {
        position = (Point) p.clone();
        dimensionActuel.setSize(dimensionDepart.getWidth()*echelle,
                dimensionDepart.getHeight()*echelle);
        notifierObservateurs();
    }

    public void setPositionDepart() {
        if (positionReset == REQUIS){
            int posX = ((int) dimensionActuel.getWidth() - source.getWidth(null))/2;
            int posY = ((int) dimensionActuel.getHeight() - source.getHeight(null))/2;
            this.position.setLocation(posX,posY);
            positionReset = false;
        }
    }

    /**
     * ajoute le deplacement decrit par le point donne en parametre a la position
     * actuelle et avertit les aobservateurs du changement
     * 
     * @param delta point contenant le deplacement en x et en y a ajouter a la
     *              position
     */
    public void deplacer(Point delta) {
        position.translate(delta.x, delta.y);
        notifierObservateurs();
    }

    /**
     * ajoute un facteur d'echelle (zoom) a la presente image et avertit les
     * observateurs. une echelle negative a pour effet de reduire le zoom
     * 
     * @param echelle facteur d'echlle a ajouter a l'image
     */
    public void ajouterEchelle(double echelle) {
        this.echelle += echelle;
        if (echelle >= 0)
            echelle = 0.1;
        dimensionActuel.setSize(dimensionDepart.getWidth()*echelle,
                                dimensionDepart.getHeight()*echelle);
        notifierObservateurs();
    }

    /**
     * cree un memento de l'ImageModifiable
     * 
     * @return memento clone a partir de l'instance
     */
    @Override
    public Memento sauvegarder() {
        try {
            var clone = (ImageModifiable) clone();
            return (Memento) clone;
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * utilise le memento passe en parametre pour restaurer un etat precedent et
     * avertit les observateurs. si c'est un memento d'ImageModifiable, récupère la
     * transformation, position, echelle. si le memento est null, reinitialise les
     * valeurs. ne fait rien si le memento n'est pas du bon type
     * 
     * @param m Memento dont on doit recuperer les valeurs.
     */
    @Override
    public void restaurer(Memento m) {
        if (m == null) {
            transformation = new AffineTransform();
            position = new Point(0, 0);
            echelle = 1;
            dimensionActuel = (Dimension) dimensionDepart.clone();
        } else if (m instanceof ImageModifiable) {
            transformation = ((ImageModifiable) m).transformation;
            position = ((ImageModifiable) m).position;
            echelle = ((ImageModifiable) m).echelle;
            dimensionActuel = ((ImageModifiable) m).dimensionActuel;
        }
        notifierObservateurs();
    }

    /**
     * avertit l'instance presente d'un changement dans l'état d'un objet observe
     */
    @Override
    public void notifier() {
        setSource(imgSource.getImageModifiee());
    }

    /**
     * ajoute un observateur au dictionnaire des observateurs. ne fait rien si
     * l'observateur y est deja.
     * 
     * @param o observateur a ajouter
     */
    @Override
    public void ajouterObservateur(Observateur o) {
        observateurs.add(o);
    }

    /**
     * retire un observateur du dictionnaire des observateurs. ne fait rien s'il n'y
     * est pas.
     * 
     * @param o observateur a retirer
     */
    @Override
    public void retirerObservateur(Observateur o) {
        observateurs.remove(o);
    }

    /**
     * notifie tous les observateurs enregistres d'un changement d'etat du present
     * objet
     */
    @Override
    public void notifierObservateurs() {
        observateurs.forEach(Observateur::notifier);
    }

    /**
     * serialise l'objet, transformant notamment l'image source en un objet
     * serialisable
     * 
     * @param oos ObjectOutputStream utilise comme destination de l'ecriture
     */
    private void writeObject(ObjectOutputStream oos) {
        try {
            oos.defaultWriteObject();
            ImageIO.write(source, "png", oos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * deserialise l'objet, recomposant notamment l'image source depuis le flux
     * d'octets
     * 
     * @param ois ObjectInputStream utilise pour lire et recomposer les objets
     */
    private void readObject(ObjectInputStream ois) {
        try {
            ois.defaultReadObject();
            source = ImageIO.read(ois);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void setDimensionDepart(Dimension dimension) {
        this.dimensionActuel = dimension;
        this.dimensionDepart = (Dimension) dimension.clone();
    }
}
