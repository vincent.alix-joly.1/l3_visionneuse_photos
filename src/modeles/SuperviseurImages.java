package modeles;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * objet federant des images modifiables, permettant de les serialiser ensemble
 */
public class SuperviseurImages extends ArrayList<ImageModifiable> {

    public static final String SUPERVISEUR_CHANGER = "CHANGER";

    /**
     * Cree un superviseur d'image présentant une vignette et un certain nombre de
     * perspectives. La vignette est stockee dans l'index 0 et utilise une image
     * comme source, tandis que les perspectives surveillent la vignette.
     * 
     * @param imageInitiale  image initiale utilisee comme source par la vignette
     * @param nbPerspectives nombre de perspectives a ajouter et qui doivent
     *                       surveiller la vignette
     */
    public SuperviseurImages(BufferedImage imageInitiale, int nbPerspectives) {
        if (nbPerspectives < 1)
            throw new IllegalArgumentException("Il faut au moins une perspective");

        add(new ImageModifiable(imageInitiale));

        for (int i = 0; i < nbPerspectives; i++) {
            add(new ImageModifiable(get(0)));
        }
    }

    /**
     * change l'image source utilisee par la vignette, ce qui avertit les
     * perspectives
     * 
     * @param image
     */
    public void changerImage(BufferedImage image) {
        get(0).setSource(image);
    }

    /**
     * vide le prsent superviseur et copie les images de celui passe en parametre
     * 
     * @param source superviseur a copier
     */
    public void copierSuperviseur(SuperviseurImages source) {
        removeAll(this);
        addAll(source);
    }
}
