package communs;

/**
 * interface signifiant qu'un objet est interesse a notifie des mises a jour
 * d'un autre objet
 */
public interface Observateur {

    /**
     * alerte l'instance d'observateur qu'un objet observe a ete mis a jour
     */
    public void notifier();
}
