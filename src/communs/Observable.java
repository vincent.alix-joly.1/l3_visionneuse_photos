package communs;

/**
 * interface permettant a un objet de diffuser son etat a d'autres objets
 * observateurs
 */
public interface Observable {

    /**
     * inscrit un observateur aux mises a jour du present observable
     * 
     * @param o observateur a inscrire
     */
    public void ajouterObservateur(Observateur o);

    /**
     * descinscrit un observateur des mises a jour de l'instance d'Observable
     * 
     * @param o observateur a desincrire
     */
    public void retirerObservateur(Observateur o);

    /**
     * alerte tous les observateurs qu'un changement a opere dans l'instance
     */
    public void notifierObservateurs();
}
