package vues;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import commandes.Commande;
import controleurs.InterpreteurAction;
import controleurs.TamponCommandes;
import modeles.SuperviseurImages;

/**
 * Barre de commandes incluant selection d'images, gestion de sauvegardes de
 * perspectives et de commandes undo/redo.
 */
public class MenuFenetre extends JMenuBar {

    private static final String MENU_FICHIER_TITRE = "Fichier";
    private static final String MENU_FICHIER_NOUVELLEIMAGE = "Nouvelle Image";
    private static final String MENU_FICHIER_CHARGERPERSP = "Charger Visionneuse";
    private static final String MENU_FICHIER_SAUVEGARDERPERSP = "Sauvegarder Visionneuse";
    private static final String MENU_FICHIER_QUITTER = "Quitter";
    private static final String MENU_COMMANDES_TITRE = "Commandes";
    private static final String MENU_COMMANDES_DEFAIRE = "Défaire";
    private static final String MENU_COMMANDES_REFAIRE = "Refaire";
    private final SuperviseurImages superviseur;

    /**
     * Cree un nouveau menu permettant d'agir sur le superviseur donne
     * 
     * @param superviseur le superviseur d'images devant être controlle
     */
    public MenuFenetre(SuperviseurImages superviseur) {
        this.superviseur = superviseur;
        ajouterMenuFichier();
        ajouterMenuCommandes();
    }

    /**
     * Créer le menu de Fichier (sauvegarde/chargement)
     */
    private void ajouterMenuFichier() {
        JMenu menuFichier = new JMenu(MENU_FICHIER_TITRE);
        JMenuItem menuNouvelle = new JMenuItem(MENU_FICHIER_NOUVELLEIMAGE);
        JMenuItem menuCharger = new JMenuItem(MENU_FICHIER_CHARGERPERSP);
        JMenuItem menuSauvegarder = new JMenuItem(MENU_FICHIER_SAUVEGARDERPERSP);
        JMenuItem menuQuitter = new JMenuItem(MENU_FICHIER_QUITTER);

        menuNouvelle.addActionListener((ActionEvent e) -> {
            JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            fileChooser.setDialogTitle("Sélectionnez une image");
            fileChooser.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter filtre = new FileNameExtensionFilter(".png", "png");
            fileChooser.addChoosableFileFilter(filtre);
            int returnValue = fileChooser.showOpenDialog(null);

            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                if (selectedFile != null) {

                    try {
                        superviseur.changerImage(ImageIO.read(selectedFile));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        menuCharger.addActionListener((ActionEvent e) -> {
            JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            fileChooser.setDialogTitle("Charger une visionneuse");
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileHidingEnabled(true);
            FileNameExtensionFilter filtre = new FileNameExtensionFilter(".ser", "ser");
            fileChooser.addChoosableFileFilter(filtre);
            int returnValue = fileChooser.showDialog(null, "Loader");

            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                if (selectedFile != null) {
                    try {
                        var fileIn = new FileInputStream(selectedFile);
                        var ois = new ObjectInputStream(fileIn);

                        superviseur.copierSuperviseur((SuperviseurImages) ois.readObject());

                        fileIn.close();
                        ois.close();

                        firePropertyChange(SuperviseurImages.SUPERVISEUR_CHANGER, null, selectedFile);

                    } catch (IOException | ClassNotFoundException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        menuSauvegarder.addActionListener((ActionEvent e) -> {
            JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
            fileChooser.setDialogTitle("Sauvegarder une visionneuse");
            fileChooser.setAcceptAllFileFilterUsed(false);
            FileNameExtensionFilter filtre = new FileNameExtensionFilter(".ser", "ser");
            fileChooser.addChoosableFileFilter(filtre);

            int returnValue = fileChooser.showSaveDialog(null); ///***

            if (returnValue == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();

                if (selectedFile != null) {
                    try {
                        var fileOut = new FileOutputStream(selectedFile + ".ser");
                        var oos = new ObjectOutputStream(fileOut);

                        oos.writeObject(superviseur);

                        fileOut.close();
                        oos.close();

                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

        menuQuitter.addActionListener((ActionEvent e) -> {
            System.exit(0);
        });

        menuFichier.add(menuNouvelle);
        menuFichier.add(menuCharger);
        menuFichier.add(menuSauvegarder);
        menuFichier.add(menuQuitter);
        add(menuFichier);

    }

    /**
     * Créer le menu de Commandes (undo/redo)
     */
    private void ajouterMenuCommandes() {
        JMenu menuCommandes = new JMenu(MENU_COMMANDES_TITRE);
        JMenuItem menuDefaire = new JMenuItem(MENU_COMMANDES_DEFAIRE);
        menuDefaire.setActionCommand(Commande.ACTION_DEFAIRE);
        menuCommandes.add(menuDefaire);
        menuDefaire.addActionListener(this::executerCommande);
        JMenuItem menuRefaire = new JMenuItem(MENU_COMMANDES_REFAIRE);
        menuRefaire.setActionCommand(Commande.ACTION_REFAIRE);
        menuCommandes.add(menuRefaire);
        menuRefaire.addActionListener(this::executerCommande);
        add(menuCommandes);
    }

    /**
     * methode appelee par les boutons undo/redo pour recuperer le tampon de
     * commandes et la commande effectuee
     * 
     * @param e evenement souleve par les boutons du menu
     */
    private void executerCommande(ActionEvent e) {
        Commande c = InterpreteurAction.getInstance().interpreterAction(e);
        if (c != null) {
            var t = TamponCommandes.getInstance();
            c.setImageModifiable(t.getDerniereImageModifiee());
            t.executerCommande(c);
        }
    }
}
