package vues;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import modeles.SuperviseurImages;

/**
 * Fenetre et point d'entree du programme
 */
public class FenetrePrincipale extends JFrame implements PropertyChangeListener {

    private static final long serialVersionUID = 1L;
    private static final String TITRE_FENETRE = "Laboratoire 3 : LOG121 - Équipe 5 - Visionneuse Photos";
    private static final Dimension DIMENSION = new Dimension(1000, 800);

    private JPanel panneauPrincipal;
    SuperviseurImages superviseur;

    /**
     * construit une fenetre principale de l'editeur d'images
     * 
     * @throws IOException
     */
    public FenetrePrincipale() throws IOException {

        superviseur = new SuperviseurImages(ImageIO.read(new File("src/vues/ch.png")), 2);
        genererPanneauPrincipal(superviseur);

        MenuFenetre menuFenetre = new MenuFenetre(superviseur);
        add(menuFenetre, BorderLayout.NORTH);
        menuFenetre.addPropertyChangeListener(this);

        // Faire en sorte que le X de la fenêtre ferme la fenêtre
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle(TITRE_FENETRE);
        setSize(DIMENSION);

        // Mettre la fenêtre au centre de l'écran
        setLocationRelativeTo(null);

        // Empêcher la redimension de la fenêtre
        setResizable(false);

        // Rendre la fenêtre visible
        setVisible(true);
    }

    /**
     * regenere le panneau principal et, par le fait meme, les vues et controleurs
     * et fonction du nouvel état du superviseur de modeles
     * 
     * @param superviseur
     */
    private void genererPanneauPrincipal(SuperviseurImages superviseur) {
        if (panneauPrincipal != null)
            remove(panneauPrincipal);
        panneauPrincipal = new PanneauPrincipal(superviseur);
        add(panneauPrincipal);
    }

    /**
     * réagit aux evement du gestionnaire de fenetrage et aux changements dans le
     * superviseur d'images
     * 
     * @param evt evenement souleve par un changement de propriete
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(SuperviseurImages.SUPERVISEUR_CHANGER)) {
            genererPanneauPrincipal(this.superviseur);
            setVisible(false);
            setVisible(true);
        }
    }

    /**
     * Demarre l'editeur
     * 
     * @param args arguments ignores
     * @throws IOException si la lecture d'images échoue
     */
    public static void main(String[] args) throws IOException {
        new FenetrePrincipale();
    }
}
