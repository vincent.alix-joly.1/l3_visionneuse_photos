package vues;

import java.awt.*;
import javax.swing.*;
import communs.Observateur;
import controleurs.ControleurImage;
import modeles.ImageModifiable;

public class VueImage extends JPanel implements Observateur {

    private final ImageModifiable modele;
    private final JPanel panelImage;
    private int posX;
    private int posY;

    public VueImage(ImageModifiable modele, ControleurImage controleur, Dimension dimension) {
        super();
        this.modele = modele;
        this.modele.ajouterObservateur(this);
        this.modele.setDimensionDepart(dimension);
        this.modele.setPositionDepart();
        panelImage = new JPanel();
        panelImage.setBackground(Color.white);
        this.add(panelImage);
        this.setLayout(null);
        this.setBackground(Color.white);
        this.setPreferredSize(dimension);
        setPanel();
        if (controleur != null) {
            panelImage.addMouseMotionListener(controleur);
            panelImage.addMouseWheelListener(controleur);
            panelImage.addMouseListener(controleur);
            panelImage.addKeyListener(controleur);
        }
    }

    private Image setPanel() {
        Image image = modele.getImageModifiee();
        posX = modele.getX();
        posY = modele.getY();
        panelImage.setLocation(posX,posY);
        panelImage.setSize(image.getWidth(this),image.getHeight(this));
        return image;
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Image image = setPanel();
        g.drawImage(image, posX,posY, panelImage);
    }

    @Override
    public void notifier() {
        super.repaint();
    }
}
