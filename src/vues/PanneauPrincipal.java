package vues;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

import controleurs.ControleurImage;
import controleurs.InterpreteurAction;
import controleurs.TamponCommandes;
import modeles.ImageModifiable;

/**
 * panneau contenant les vues et disposant les fenêtres aux bons endroits
 */
public class PanneauPrincipal extends JPanel {

    // les panneaux pX sont des "fillers" pour le layout

    /**
     * Construit un panneau a une vignette et deux perspectives
     * 
     * @param modeles liste des modeles dont devant etre visibles grâce à une vue.
     *                le modele 0 est une vue sans controleur (une vignette)
     */
    public PanneauPrincipal(List<ImageModifiable> modeles) {

        JPanel p1, p2, p3, p4, p5, p21, p23, p25, p41, p42, p44, p45;
        Dimension dimensionVignette = new Dimension(225, 225);
        Dimension dimensionPerspective = new Dimension(325, 325);
        ControleurImage controleurImage1 =  new ControleurImage(modeles.get(1),
                                            InterpreteurAction.getInstance(),
                                            TamponCommandes.getInstance());
        ControleurImage controleurImage2 =  new ControleurImage(modeles.get(2),
                                            InterpreteurAction.getInstance(),
                                            TamponCommandes.getInstance());

        VueImage vignette = new VueImage(modeles.get(0), null, dimensionVignette);
        VueImage perspective1 = new VueImage(modeles.get(1),controleurImage1, dimensionPerspective);
        VueImage perspective2 = new VueImage(modeles.get(2),controleurImage2, dimensionPerspective);

        this.setBackground(Color.black);

        p1 = new JPanel();
        p2 = new JPanel();
        p3 = new JPanel();
        p4 = new JPanel();
        p5 = new JPanel();

        p1.setPreferredSize(new Dimension(1000, 50));
        p2.setPreferredSize(new Dimension(1000, 325));
        p3.setPreferredSize(new Dimension(1000, 30));
        p4.setPreferredSize(new Dimension(1000, 225));
        p5.setPreferredSize(new Dimension(1000, 70));

        p1.setBackground(Color.black);
        p2.setBackground(Color.black);
        p3.setBackground(Color.black);
        p4.setBackground(Color.black);
        p5.setBackground(Color.black);

        p21 = new JPanel();
        p23 = new JPanel();
        p25 = new JPanel();

        p21.setPreferredSize(new Dimension(50, 325));
        perspective1.setPreferredSize(new Dimension(325, 325));
        p23.setPreferredSize(new Dimension(225, 325));
        perspective2.setPreferredSize(new Dimension(325, 325));
        p25.setPreferredSize(new Dimension(50, 325));

        p21.setBackground(Color.black);
        perspective1.setBackground(Color.white);
        p23.setBackground(Color.black);
        perspective2.setBackground(Color.white);
        p25.setBackground(Color.black);

        p2.add(p21);
        p2.add(perspective1);
        p2.add(p23);
        p2.add(perspective2);
        p2.add(p25);

        p41 = new JPanel();
        p42 = new JPanel();
        p44 = new JPanel();
        p45 = new JPanel();

        p41.setPreferredSize(new Dimension(50, 225));
        p42.setPreferredSize(new Dimension(325, 225));
        vignette.setPreferredSize(new Dimension(225, 225));
        p44.setPreferredSize(new Dimension(325, 225));
        p45.setPreferredSize(new Dimension(50, 225));

        JLabel titrePerspective1 = new JLabel("Perspective 1", JLabel.CENTER);
        titrePerspective1.setFont(new Font("Serif", Font.BOLD, 30));
        titrePerspective1.setForeground(Color.white);
        p42.add(titrePerspective1);

        JLabel titrePerspective2 = new JLabel("Perspective 2", JLabel.CENTER);
        titrePerspective2.setFont(new Font("Serif", Font.BOLD, 30));
        titrePerspective2.setForeground(Color.white);
        p44.add(titrePerspective2);

        JLabel titreVignette = new JLabel("Vignette", JLabel.CENTER);
        titreVignette.setFont(new Font("Serif", Font.BOLD, 30));
        titreVignette.setForeground(Color.white);
        p5.add(titreVignette);

        p41.setBackground(Color.black);
        p42.setBackground(Color.black);
        vignette.setBackground(Color.white);
        p44.setBackground(Color.black);
        p45.setBackground(Color.black);

        p4.add(p41);
        p4.add(p42);
        p4.add(vignette);
        p4.add(p44);
        p4.add(p45);

        this.add(p1);
        this.add(p2);
        this.add(p3);
        this.add(p4);
        this.add(p5);
        this.setVisible(true);
    }

} 